kafka
=====

![logo.png](logo.jpeg)

### description

A simple `kafka` / `docker-compose` mockup stack

### requirements

- `docker`
- `docker-compose`

### usage

#### start

    # docker-compose up --build --remove-orphans

#### scale services

Each service can be scaled independently using environment variables _(which all default to 1 if unspecified)_

    # brokers=3    docker-compose up --build --remove-orphans
    # consumers=3  docker-compose up --build --remove-orphans
    # producers=3  docker-compose up --build --remove-orphans
    # zookeepers=3 docker-compose up --build --remove-orphans

    ## scale all services simultaneously

    # zookeepers=3 brokers=3 consumers=3 producers=3 docker-compose up --build --remove-orphans

#### data persistence

**By default, data** _(kafka & zookeeper logs)_ **is not persisted locally**

Everything gets written to `/tmp` _(eg. the default behaviour of both kafka & zookeeper)_

However this can be by overridden through the `${data}` environment variable _(which defaults to `tmp` as per the above)_

This can be achieved by targeting the `/data` external volume which writes to the `./data` directory _(included in .gitignore)_

    # data=data docker-compose up --build --remove-orphans

#### jmxterm

`Dockerfile` includes `jmxterm` in order to ease debugging as well as enable kafka broker healthcheck

##### beans

Fetch the list of beans exposed through `JMX` in the container named `kafka-broker-1`

    # docker exec kafka-broker-1 sh beans

##### healthcheck

Retrieve a broker's state

    # docker exec kafka-broker-1 sh healthcheck && echo UP || echo DOWN
    UP

#### sample output

###### start

    kafka-consumer-1   | {"timestamp":"2021-11-04T08:34:55+00:00","producer":"881972e596c3","interval":3.333,"message":13059}
    kafka-consumer-1   | {"timestamp":"2021-11-04T08:34:59+00:00","producer":"881972e596c3","interval":3.333,"message":10715}
    kafka-consumer-1   | {"timestamp":"2021-11-04T08:35:02+00:00","producer":"881972e596c3","interval":3.333,"message":1889}
    kafka-consumer-1   | {"timestamp":"2021-11-04T08:35:05+00:00","producer":"881972e596c3","interval":3.333,"message":14243}

###### beans

    kafka.server:type=ControllerMutation
    kafka.server:type=Fetch
    kafka.server:type=Produce
    kafka.server:type=Request
    kafka.server:type=app-info
    kafka.server:type=group-coordinator-metrics
    kafka.server:type=kafka-metrics-count
    kafka.server:type=socket-server-metrics
    kafka.server:type=transaction-coordinator-metrics
    kafka.server:type=txn-marker-channel-metrics

###### docker ps

    # docker ps | sort
    {"name":"kafka-broker-1","id":"cfbaadffd9db","status":"Up 2 minutes (healthy)"}
    {"name":"kafka-consumer-1","id":"45cb588d27b2","status":"Up 2 minutes"}
    {"name":"kafka-producer-1","id":"881972e596c3","status":"Up 2 minutes"}
    {"name":"kafka-zookeeper-1","id":"2d028476f609","status":"Up 2 minutes"}

    # note : the above json formatted output requires the following psFormat in docker config.json
    "psFormat" : "{\"name\":\"{{.Names}}\",\"id\":\"{{.ID}}\",\"status\":\"{{.Status}}\"}"

###### docker stats

    # docker stats --no-stream | sort
    {"name":"kafka-broker-1","cpu":"5.65%","mem":"84.12%","memusage":"336.5MiB / 400MiB"}
    {"name":"kafka-consumer-1","cpu":"0.74%","mem":"82.38%","memusage":"82.38MiB / 100MiB"}
    {"name":"kafka-producer-1","cpu":"0.47%","mem":"79.64%","memusage":"79.64MiB / 100MiB"}
    {"name":"kafka-zookeeper-1","cpu":"0.52%","mem":"83.86%","memusage":"83.86MiB / 100MiB"}

    # note : the above json formatted output requires the following statsFormat in docker config.json
    "statsFormat" : "{\"name\":\"{{.Name}}\",\"cpu\":\"{{.CPUPerc}}\",\"mem\":\"{{.MemPerc}}\",\"memusage\":\"{{.MemUsage}}\"}"
